using Microsoft.Data.SqlClient;
public class ClienteRepository : Database, IClienteRepository
{
    public void Create(Cliente cliente)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "INSERT INTO Cliente VALUES (@name, @email, @password)";

        cmd.Parameters.AddWithValue("@name",     cliente.Name);
        cmd.Parameters.AddWithValue("@email",    cliente.Email);
        cmd.Parameters.AddWithValue("@password", cliente.Password);

        cmd.ExecuteNonQuery();
    }

    public void Delete(int id)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "DELETE FROM Cliente WHERE ClienteId = @id";

        cmd.Parameters.AddWithValue("@id", id);

        cmd.ExecuteNonQuery();
    }

    public List<Cliente> Read()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Cliente";

        SqlDataReader reader = cmd.ExecuteReader();
        List<Cliente> clientes = new List<Cliente>();

        while(reader.Read())
        {
            Cliente c = new Cliente();
            
            c.ClienteId =   reader.GetInt32(0);
            c.Name =        reader.GetString(1);
            c.Email =       reader.GetString(2);
            c.Password =    reader.GetString(3);

            clientes.Add(c); 
        }

        return clientes;
    }

    public Cliente Read(int id)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Cliente WHERE ClienteId = @id";
        cmd.Parameters.AddWithValue("@id", id);

        SqlDataReader reader = cmd.ExecuteReader();

        if(reader.Read())
        {
            Cliente c = new Cliente();

            c.ClienteId =   reader.GetInt32(0);
            c.Name =        reader.GetString(1);
            c.Email =       reader.GetString(2);
            c.Password =    reader.GetString(3);

            return c;
        }
        return null;
    }

    public List<Cliente> Search(string busca)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Cliente WHERE Name LIKE @busca";
        cmd.Parameters.AddWithValue("@busca", $"%{busca}%");

        SqlDataReader reader = cmd.ExecuteReader();
        List<Cliente> clientes = new List<Cliente>();

        while(reader.Read())
        {
            Cliente c = new Cliente();
            
            c.ClienteId =   reader.GetInt32(0);
            c.Name =        reader.GetString(1);
            c.Email =       reader.GetString(2);
            c.Password =    reader.GetString(3);

            clientes.Add(c); 
        }

        return clientes;
    }

    public void Update(Cliente cliente, int id)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = @"UPDATE Cliente 
                            SET Name = @name,
                            Email = @email,
                            Password = @password
                            WHERE ClienteId = @id";

        cmd.Parameters.AddWithValue("@name",        cliente.Name);
        cmd.Parameters.AddWithValue("@email",       cliente.Email);
        cmd.Parameters.AddWithValue("@password",    cliente.Password);
        cmd.Parameters.AddWithValue("@id",          id);

        cmd.ExecuteNonQuery();
    }
}