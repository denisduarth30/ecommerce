public interface IClienteRepository
{
    //Definir quais são os métodos
    List<Cliente> Read(); //Ler todos os dados da minha lista de produtos
    Cliente Read(int id); //Retorna um produto pelo seu id
    void Create(Cliente cliente); //Metodo de criação de produtos
    void Delete(int id);
    void Update(Cliente cliente, int id);
    List<Cliente> Search(string busca);
}