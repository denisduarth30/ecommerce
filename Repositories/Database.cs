using Microsoft.Data.SqlClient; //Não é mais incorporado ao .NET Framework desde a versão 5.0

public abstract class Database : IDisposable //IDisposable -> fecha a conexão automaticamente quando não é mais necessária
{
    protected SqlConnection conn; //Protected -> public para quem herda, privado para todo o resto
    
    //abrir a conexão
    public Database()
    {
        string connectionString = @"Data Source=DESKTOP-A7IQO85\\SQLDENIS; 
        Initial Catalog=BDEcommerce; 
        Integrated Security=true; 
        TrustServerCertificate=true";
        conn = new SqlConnection(connectionString); //Usar string de conexão
        conn.Open();

        Console.WriteLine("Conexão aberta");
    }

    //fechar a conexão
    public void Dispose()
    {
        conn.Close();
        Console.WriteLine("Conexão fechada");
    }
}