/*
    Três classes para mexer com banco de dados

    SqlConnection -> Conectar
    SqlCommand -> Realizar comandos SQL
    SqlDataReader -> Fazer consultas
*/
using Microsoft.Data.SqlClient;

//Implementação para usar o banco de dadosa o invés de memória
public class ProdutoRepository : Database, IProdutoRepository
{
    public void Create(Produto produto)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "INSERT INTO Produto VALUES (@nome, @preco)"; //erro SQL Inject -> To pegando um texto e to concatenando com a string

        cmd.Parameters.AddWithValue("@nome", produto.Nome);
        cmd.Parameters.AddWithValue("@preco", produto.Preco);

        cmd.ExecuteNonQuery(); //Execução sem consulta (Inserir, Alterar, Apagar) e retorna um número inteiro (quantidade de linhas afetadas)
    }

    public void Delete(int id)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "DELETE FROM Produto WHERE ProdutoId = @id";

        cmd.Parameters.AddWithValue("@id", id);

        cmd.ExecuteNonQuery();
    }

    public List<Produto> Read()
    {
        /*
            Todo comando SQL deve ser enviado através de uma conexão
        */
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Produto";

        /*
            Usar a conexão, vai mandar o comando e executar o comando no SQL Server. 
            O Comando irá retornar os dados que consultei
        */

        SqlDataReader reader = cmd.ExecuteReader(); // Cursor de leitura dos dados 

        List<Produto> produtos = new List<Produto>();

        /*
            Loop While para trazer os dados inseridos
        */

        while(reader.Read()) // Read() -> Verificar se existe um registro e referenciar ele
        {
            Produto p = new Produto();

            p.ProdutoId =   reader.GetInt32(0);     // Trazer a primeira coluna (coluna 0)
            p.Nome =        reader.GetString(1);    // Trazer a segunda  coluna (coluna 1)
            p.Preco =       reader.GetDecimal(2);   // Trazer a terceira coluna (coluna 2)

            produtos.Add(p);
        }

        // Dispose();

        return produtos;
    }

    public Produto Read(int id)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Produto WHERE ProdutoId = @id";

        cmd.Parameters.AddWithValue("@id", id);

        SqlDataReader reader = cmd.ExecuteReader(); 

        if(reader.Read()) 
        {
            Produto p = new Produto();

            p.ProdutoId =   reader.GetInt32(0);     // Trazer a primeira coluna (coluna 0)
            p.Nome =        reader.GetString(1);    // Trazer a segunda  coluna (coluna 1)
            p.Preco =       reader.GetDecimal(2);   // Trazer a terceira coluna (coluna 2)
            
            return p;
        }
        return null;
    }

    public void Update(Produto produto, int id)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = @"UPDATE Produto 
                            SET Nome = @nome, 
                            Preco = @preco
                            WHERE ProdutoId = @id";

        cmd.Parameters.AddWithValue("@id", id);
        cmd.Parameters.AddWithValue("@nome", produto.Nome);
        cmd.Parameters.AddWithValue("@preco", produto.Preco);

        cmd.ExecuteNonQuery();
    }
}