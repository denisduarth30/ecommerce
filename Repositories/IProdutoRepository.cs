public interface IProdutoRepository
{
    //Definir quais são os métodos
    List<Produto> Read(); //Ler todos os dados da minha lista de produtos
    Produto Read(int id); //Retorna um produto pelo seu id
    void Create(Produto produto); //Metodo de criação de produtos
    void Delete(int id);
    void Update(Produto produto, int id);
}