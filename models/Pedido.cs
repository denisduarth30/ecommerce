public class Pedido{
    
    public int PedidoId { get; set; }
    public DateTime Data { get; set; }
    public List<Item> Items { get; set; }
    public Cliente Cliente {get; set;}
    public decimal Total => Items.Sum(item => item.Total); //modo mais fácil (usando arrow function)

    // public decimal Total {
        // get{
        //   return Items.Sum(item => item.Total);
        // }    

        // get
        // {
        //     decimal total = 0;
        //     foreach (Item item in Items)
        //     {
        //         total += item.Total;
        //     }
        //     return total;
        // }

    
}