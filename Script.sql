CREATE DATABASE BDEcommerce
GO

USE BDEcommerce
GO

/*
------------------------------------------------------------------
							TABELAS
------------------------------------------------------------------
*/
CREATE TABLE Produto
(
	ProdutoId	INT				NOT NULL PRIMARY KEY IDENTITY,
	Nome		VARCHAR(200)	NOT NULL,
	Preco		DECIMAL(9,2)	NOT NULL
)
GO

CREATE TABLE Cliente
(
	ClienteId	INT				NOT NULL	PRIMARY KEY IDENTITY,
	Name		VARCHAR(200)	NOT NULL,
	Email		VARCHAR(200)	NOT NULL,
	Password	VARCHAR(200)	NOT NULL
)
GO

/*
------------------------------------------------------------------
							INSERTS
------------------------------------------------------------------
*/
INSERT INTO Produto 
VALUES	('L�pis',	1.50),
		('Caneta',	2.50),
		('Caderno', 7.20)
GO

INSERT INTO Cliente
VALUES	('Denis',	'denis@denis.com',		'12345'),
		('Deizy',	'deizy@deizy.com',		'12345'),
		('Igor',	'igor@igor.com',		'12345'),
		('Gustavo',	'gustavo@gustavo.com',	'12345'),
		('Andr�',	'andre@andre.com',		'12345')
GO

