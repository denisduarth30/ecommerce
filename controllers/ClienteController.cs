//Tem que ter o sufixo "controller" no final dos arquivos
using Microsoft.AspNetCore.Mvc; //Sessions, rotas etc. tudo para enviar dados, receber dados etc.

public class ClienteController : Controller{
    IClienteRepository clienteRepository;

    public ClienteController(IClienteRepository clienteRepository)
    {
        this.clienteRepository = clienteRepository;
    }

    public ActionResult Index() //página inicial da Classe ClienteController
    {
        return View(clienteRepository.Read()); // Retorna a view (página) da lista de clientes 
    }    

    [HttpGet]
    public ActionResult Create()
    {
        return View();
    }
    
    [HttpPost]
    public ActionResult Create(Cliente model)
    {
        clienteRepository.Create(model);
        return RedirectToAction("Index"); //Redireciona para a action (método) "Index" (página inicial) da classe ClienteController
    }
    
    public ActionResult Details(int id)
    {
        Cliente c = clienteRepository.Read(id);

        if(c != null)
        {
            return View(c);
        }
        return NotFound();
    }

    public ActionResult Delete(int id)
    {
        clienteRepository.Delete(id);
        return RedirectToAction("Index");
    }

    [HttpGet]
    // Action result para retornar a página de update de um cliente da lista
    public ActionResult Update(int id)
    {
        Cliente c = clienteRepository.Read(id);

        if(c != null)
        {
            return View(c);
        }
        return NotFound();
    }

    [HttpPost]
    /*
        Action result para editar os dados de um cliente da lista
        e retornar para a página inicial da classe ClienteController
    */
    public ActionResult Update(int id, Cliente cliente)
    {
        clienteRepository.Update(cliente, id);
        return RedirectToAction("Index");
    }

    [HttpPost]
    public ActionResult Search(string busca)
    {
       var lista  = clienteRepository.Search(busca);
       return View("Index", lista);
    }
}