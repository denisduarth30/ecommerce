using Microsoft.AspNetCore.Mvc;

public class ProdutoController : Controller
{

    IProdutoRepository produtoRepository;

    public ProdutoController(IProdutoRepository produtoRepository)
    {
        this.produtoRepository = produtoRepository;
    }

    public ActionResult Index()                 // http://localhost:XXXX/produto/index
    {
        return View(produtoRepository.Read());  // Views/Produto/Index.cshtml
    }
    
    [HttpGet]
    public ActionResult Create()    // http://localhost:XXXX/produto/create
    { 
        return View();              // Views/Produto/Create.cshtml
    }

    [HttpPost]
    public ActionResult Create(Produto model) //IFormColletion form
    {
        produtoRepository.Create(model);
        return RedirectToAction("Index");
    }

    
    public ActionResult Details(int id) //http://localhost:XXXX/produto/details/id? (? => opcional)
    {
        Produto p = produtoRepository.Read(id);

        if (p != null)
        {
            return View(p);
        }
        
        return NotFound();
    }

    public ActionResult Delete(int id)
    {
        produtoRepository.Delete(id);
        return RedirectToAction("Index");
    }

    [HttpGet]
    public ActionResult Update(int id)
    {
        Produto p = produtoRepository.Read(id);

        if (p != null)
        {
            return View(p);
        }
        return NotFound();
    }
    
    [HttpPost]
    public ActionResult Update(Produto produto, int id)
    {
        produtoRepository.Update(produto, id);
        return RedirectToAction("Index");
    }

    /*
        [HttpPost]
        
        public ActionResult Search(IFormCollection form)
        {
        string busca = form["busca"];
        List<Produto> listaBusca = new List<Produto>();

        foreach (Produto p in lista)
        {
                if (busca != null)
                {
                    if(p.Nome.Contains(busca))
                    {
                        listaBusca.Add(p);
                    }
                }
        } 
            return View("Index", listaBusca);
        }
    */
}