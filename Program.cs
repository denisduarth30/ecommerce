var builder = WebApplication.CreateBuilder(args); //Cria o servidor

/*
    add middlewares
    builder.Services.AddControllers();
*/

builder.Services.AddControllersWithViews();

/*
    Adicionar implementação na interface. Quando eu quero que essa classe seja instanciada em cada requisição
    
    builder.Services.AddTransient<IProdutoRepository, ProdutoMemoryRepository>(); 
*/

/*
    Todas vez que eu chamar o IProdutoRepository, na verdade use os metódos do ProdutoMemoryRepository
*/
// Manter uma conexão durante todas uma transição, reutilização
builder.Services.AddTransient<IProdutoRepository, ProdutoRepository>(); 
builder.Services.AddTransient<IClienteRepository, ClienteRepository>(); 

var app = builder.Build();
/* 
    app.MapGet("/", () => ""); //Raiz - Retorna o caminho "Hello World"
    app.MapControllers(); //using [Route("")]
*/

//setup middlewares
app.MapControllerRoute("default", "/{controller=Cliente}/{action=Index}/{id?}");
app.Run();
